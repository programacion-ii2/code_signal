/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.ej;

/**
 *
 * @author USUARIO
 */

import java.util.Arrays;
import java.util.stream.IntStream;
public class App {
  public static boolean solution (int[] a, int[] b) {
    
 int contador = 0;
        int indice = -1;
        if (IntStream.of(a).sum() != IntStream.of(b).sum()) {
            return false;
        }
        for (int i = 0; i < a.length; i++) {
            if (a[i] != b[i]) {
                contador++;
                if (contador  == 2 && a[i]!=b[indice] || contador>2)
                        return false;
                indice = i;
            }
        }
        return true;
}
}