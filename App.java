/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.ej;

/**
 *
 * @author USUARIO
 */
import java.util.Scanner;
public class App {
    public static boolean solution(int n) {
        String s = Integer.toString(n);
        int mid = s.length() / 2;
        int sumLeft = 0;
        int sumRight = 0;
        for (int i = 0; i < mid; i++) {
            sumLeft += s.charAt(i) - '0';
        }
        for (int i = mid; i < s.length(); i++) {
            sumRight += s.charAt(i) - '0';
        }
        return sumLeft == sumRight;
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(solution(n));
    }
}
