/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.ej7;

/**
 *
 * @author USUARIO
 */ 
import java.util.*;
public class Ej7 {

    public static boolean solution(int[] sequence) {
        int count = 0;
        for (int i = 1; i < sequence.length; i++) {
            if (sequence[i] <= sequence[i - 1]) {
                count++;
                if (count > 1) {
                    return false;
                }
                if (i > 1 && sequence[i] <= sequence[i - 2]) {
                    sequence[i] = sequence[i - 1];
                }
            }
        }
        return true;
    }
    public static void main(String[] args) {
        int[] sequence = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        System.out.println(solution(sequence)); 
        sequence = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        System.out.println(solution(sequence));  
    }
}

