/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.ej;

/**
 *
 * @author USUARIO
 */


   
public class App{
    public static int solution(int[] inputArray) {
        int suma = 0;
        for(int i = 1; i < inputArray.length; i++){
            if(inputArray[i] <= inputArray[i-1]){
                int d = inputArray[i-1] - inputArray[i] + 1;
                suma += d;
                inputArray[i] = inputArray[i] + d;
            }
        }
        return suma;
    }
    public static void main(String[] args) {
        int[] inputArray = {1, 3, 2, 1, 2, 1};
        int result = solution(inputArray);
        System.out.println(result); 
    }
}


