/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.veiniteYUnoej;

/**
 *
 * @author USUARIO
 */

  
import java.util.regex.*;
public class venteYuno_ej {
    private static final String IPv4_REGEX = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$";
    public static boolean isValidIPv4Address(String ipAddress) {
        Pattern pattern = Pattern.compile(IPv4_REGEX);
        Matcher matcher = pattern.matcher(ipAddress);
        return matcher.matches();
    }
    public static void main(String[] args) {
        String[] ipAddresses = {"192.168.1.1", "192.168.0.256", "192.168.a.1", "192.168.1.1.1"};
        for (String ipAddress : ipAddresses) {
            System.out.println(ipAddress + " is valid: " + isValidIPv4Address(ipAddress));
        }
    }
}
