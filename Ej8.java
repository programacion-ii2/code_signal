/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.ej8;

/**
 *
 * @author USUARIO
 */
import java.util.*;
public class Ej8 {
    public static int solution(int[][] matrix) {
        int suma_matrix = 0;
        int filas = matrix[0].length; // la longitud de una matriz 
        int condicion = 0;
        for (int i = 0; i < filas; i++) {
            for (int j = 0; j < matrix.length; j++) {
                condicion = matrix[j][i];
                if (condicion == 0) { // coomparaccion a  0 
                    break;
                }
                suma_matrix += condicion;
            }
        }
        return suma_matrix;
    }
    public static void main(String[] args) {
        int[][] sequence = {{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
                             {11, 12, 13, 14, 15, 16, 17, 18, 19, 20}};
        System.out.println(solution(sequence)); // Output: 210
    }
}