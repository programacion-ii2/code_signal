/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.ej6;

/**
 *
 * @author USUARIO
 */
import java.util.*;
public class Ej6{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // Leer el número de estatuas
        int n = scanner.nextInt();
        // Leer las alturas de las estatuas
        int[] statues = new int[n];
        for (int i = 0; i < n; i++) {
            statues[i] = scanner.nextInt();
        }
        // Encontrar la estatua más alta y la más baja
        int max = statues[0];
        int min = statues[0];
        for (int i = 1; i < n; i++) {
            if (statues[i] > max) {
                max = statues[i];
            }
            if (statues[i] < min) {
                min = statues[i];
            }
        }
        // Calcular el número de estatuas que hay que añadir
        int count = (max - min + 1) - n;
        // Imprimir el resultado
        System.out.println(count);
    }
}