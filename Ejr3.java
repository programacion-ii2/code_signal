/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.ejr3;

/**
 *
 * @author USUARIO
 */
import java.util.Scanner;
public class Ejr3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // Leer la primera cadena
        String s1 = scanner.nextLine();
        // Leer la segunda cadena
        String s2 = scanner.nextLine();
        // Contar el número de caracteres comunes
        int count = 0;
        for (int i = 0; i < s1.length(); i++) {
            char c = s1.charAt(i);
            if (s2.indexOf(c) != -1) {
                count++;
            }
        }
        // Imprimir el resultado
        System.out.println(count);
    }
}