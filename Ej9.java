/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.ej9;

/**
 *
 * @author USUARIO
 */
import java.util.ArrayList;
public class Ej9 {
    public static void main(String[] args) {
        String[] inputArray = {"abc", "de", "fgh", "de", "ijkl", "mn", "o"};
        String[] solutionArray = solution(inputArray);

        for (String s : solutionArray) {
            System.out.println(s);
        }
    }

    public static String[] solution(String[] inputArray) {
        int maxLength = 0;
        ArrayList<String> longestStrings = new ArrayList<>();

        for (int i = 0; i < inputArray.length; i++) {
            if (inputArray[i].length() > maxLength) {
                maxLength = inputArray[i].length();
                longestStrings.clear();
                longestStrings.add(inputArray[i]);
            } else if (inputArray[i].length() == maxLength) {
                longestStrings.add(inputArray[i]);
            }
        }

        return longestStrings.toArray(new String[0]);
    }
}