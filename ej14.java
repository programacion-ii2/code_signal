/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.ej;

/**
 *
 * @author USUARIO
 */
import java.util.Scanner;
public class App{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print(" ingrese el numero de elemetos : ");
        int n = scanner.nextInt();

        System.out.print("ingrese los elememtos : ");
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }

        int[] weights_person = new int[2];

        for (int indexA = 0; indexA < arr.length; indexA++) {
            weights_person[indexA % 2] += arr[indexA];
        }

        System.out.println(" peso de las personas : " + weights_person[0] + ", " + weights_person[1]);

        scanner.close();
    }
}

