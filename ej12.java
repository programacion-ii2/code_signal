/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.ej;

/**
 *
 * @author USUARIO
 */


import java.util.Arrays;
public class App {
    public static void main( String  []  args) {
        float [] a = {1.70f , 1.65f , 1.50f , 1.40f};
        sortArray(a);
        System.out.println(Arrays.toString(a)); 
    }
    public static void sortArray(float [] a) {
        for (int i = 0; i < a.length; i++) {
            for (int j = i + 1; j < a.length; j++) {
                if (a[i] > a[j] && a[i] != -1 && a[j] != -1) {
                    float temp = a[i];
                    a[i] = a[j];
                    a[j] = temp;
                }
            }
        }
    }
}