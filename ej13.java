/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.ej;

/**
 *
 * @author USUARIO
 */


public class App {
    public static void main(String[] args) {
        String inputString = "(()())";
        System.out.println(reverseParentheses(inputString));
    }

    public static String reverseParentheses(String inputString) {
        StringBuilder str = new StringBuilder(inputString);
        int start, end;
        while (str.indexOf("(") != -1) {
            start = str.lastIndexOf("(");
            end = str.indexOf(")", start);
            str.replace(start, end + 1, new StringBuilder(str.substring(start + 1, end)).reverse().toString());
        }
        return str.toString();
    }
}

