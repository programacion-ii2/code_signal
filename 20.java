/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.ej;

/**
 *
 * @author USUARIO
 */


   import java.util.ArrayList;
import java.util.Collections;
public class App{
    public static int solution(int[] inputArray) {
        ArrayList<Integer> al = new ArrayList<Integer>();
        for (int i = 0; i < inputArray.length - 1; i++) {
            int h = Math.abs(inputArray[i] - inputArray[i + 1]);
            al.add(h);
        }
        Collections.sort(al);
        return (al.get(al.size() - 1));
    }
}
