/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.ej;

/**
 *
 * @author USUARIO
 */


    import java.util.*;
public class App {
    public static boolean palindromo(String str) {
        
        Set<Character> charSet = new HashSet<>();
        for (char c : str.toCharArray()) {
            charSet.add(c);
        }
        
        if (charSet.size() % 2 == 1) {
            return false;
        }
        return true;
    }
    public static void main(String[] args) {
        String str1 = "oso";
        String str2 = "anilina";
        System.out.println(palindromo(str1)); 
        System.out.println(palindromo(str2)); 
    }
}


