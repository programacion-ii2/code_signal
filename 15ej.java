/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.ej;

/**
 *
 * @author USUARIO
 */
import java.util.Arrays;
public class App {
  public static void main(String[] args) {
    char[][] matrix = {
      {'a', 'b', 'c'},
      {'d', 'e', 'f'},
      {'g', 'h', 'i'}
    };
    char[][] newMatrix = addBorder(matrix);
    for (char[] row : newMatrix) {
      System.out.println(Arrays.toString(row));
    }
  }
  public static char[][] addBorder(char[][] matrix) {
      
    char[][] newMatrix = new char[matrix.length + 2][matrix[0].length + 2];
    for (int i = 0; i < newMatrix[0].length; i++) {
      newMatrix[0][i] = '*';
    }
    
    for (int i = 0; i < newMatrix[0].length; i++) {
      newMatrix[newMatrix.length - 1][i] = '*';
    }
    
    for (int i = 0; i < newMatrix.length; i++) {
      newMatrix[i][0] = '*';
    }
    // Añade una columna de asteriscos (*) al lado derecho de la matriz.
    for (int i = 0; i < newMatrix.length; i++) {
      newMatrix[i][newMatrix[0].length - 1] = '*';
    }
    
    for (int i = 1; i < newMatrix.length - 1; i++) {
      for (int j = 1; j < newMatrix[0].length - 1; j++) {
        newMatrix[i][j] = matrix[i - 1][j - 1];
      }
    }
    return newMatrix;
  }
}

