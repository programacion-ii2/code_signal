/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.ej4;

/**
 *
 * @author USUARIO
 */

import java.util.*;

public class Ej4 {
    public static int maxProduct(int[] inputArray) {
        
        int product = inputArray[0] * inputArray[1];
        
        for (int i = 2; i < inputArray.length; i++) {
           
            int a = inputArray[i] * inputArray[i + 1];
            if (a > product) {
                product = a;
            }
        }
        
        return product;
    }
    public static void main(String[] args) {
        int[] inputArray = {1, 2, 3, 4, 5};
        System.out.println(maxProduct(inputArray)); 
    }
}